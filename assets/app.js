/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';

export class LoginIcon extends React.Component {
  render() {
    return (
      <PlayArrowIcon style={{ color: 'white' }}/>
    );
  }
}

ReactDOM.render(<LoginIcon/>, document.getElementById('loginIcon'));
