import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { PlayArrow, PlaylistPlay } from '@material-ui/icons';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';


class Add extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      'name': ''
    }
    this.handleClick = this.handleClick.bind(this);
    var name = document.querySelector('#add').dataset.name;
    this.state.name = name;
  }

  handleClick(event) {
    window.location = '/add/' + this.state.name;
  }

  render() {
    return (
      <Fab color="primary" aria-label="add" onClick={this.handleClick}>
        <AddIcon/>
      </Fab>
    );
  }
}

class Hint extends React.Component {
  render() {
    return (
      <div style={{ marginBottom: '20px' }}>
        <Alert severity="info">
          <Typography variant="h5">
            Visit <a href="http://aschoch.ch:8000">http://aschoch.ch:8000</a> to enter your own music!
          </Typography>
        </Alert>
      </div>
    );
  }
}

class Playing extends React.Component {
  constructor(props) {
    super(props);
    var playing = JSON.parse(document.querySelector('#playing').dataset.playing);
    const eventSource2 = new EventSource('http://localhost:3000/.well-known/mercure?topic=' + encodeURIComponent('/playing'));
    eventSource2.onmessage = event => {
      console.log('Update Playing!');
      this.setState({
        playing: JSON.parse(event.data),
      });
      this.forceUpdate()
    };
    this.state = {
      playing: playing,
    };
  }

  render() {
    if( window.screen.width > 1280 ) {
      return(
        <div>
          <div>
            <Typography variant="h4" style={{ marginTop: '50px', marginBottom: '50px', color: 'white' }}>
              <Button variant="outlined" style={{ borderColor: 'white', marginRight: '20px' }}>
                <PlayArrow fontSize="large" style={{ color: 'white' }}/>
              </Button>
              Currently Playing
            </Typography>
            <DesktopCard entry={ this.state.playing }/>
          </div>
        </div>
      );
    }

    return (
      <div>
          <Typography variant="h4" style={{ marginTop: '50px', marginBottom: '50px' }}>
            <Button variant="outlined" style={{ borderColor: 'white', marginRight: '20px' }}>
              <PlayArrow fontSize="large" style={{ color: 'white' }}/>
            </Button>
            Currently Playing
          </Typography>
          <MobileCard entry={ this.state.playing }/>
      </div>
    );
  }
}

class Queue extends React.Component {
  constructor(props) {
    super(props);
    var entries = Object.values(JSON.parse(document.querySelector('#queue').dataset.entries));
    var button = document.querySelector('#queue').dataset.button;
    const eventSource = new EventSource('http://localhost:3000/.well-known/mercure?topic=' + encodeURIComponent('/entries'));
    eventSource.onmessage = event => {
      this.setState({
        entries: Object.values(JSON.parse(event.data)),
        button: this.state.button,
      });
    };
    this.state = {
      entries: entries,
      button: button,
    };
    console.log(this.state.button);
  }

  render() {
    if( window.screen.width > 1280 ) {
      return (
        <div style={{ marginBottom: '20px' }}>
          <Typography variant="h4" style={{ marginTop: '50px', marginBottom: '50px' }}>
            <Button variant="outlined" style={{ marginRight: '20px' }}>
              <PlaylistPlay fontSize="large"/>
            </Button> 
            Queue
          </Typography>
          {this.state.entries.map(entry => (
            <DesktopCard entry={ entry } key={ entry.title }/>
          ))}
        </div>
      );
    }

    return (
      <div>
          <Typography variant="h4" style={{ marginTop: '50px', marginBottom: '50px' }}>
            <Button variant="outlined" style={{ marginRight: '20px' }}>
              <PlaylistPlay fontSize="large"/>
            </Button> 
            Queue
          </Typography>
        {this.state.entries.map(entry => (
          <MobileCard entry={ entry }  key={ entry.title }/>
        ))}
      </div>
    );
  }
}

const MobileCard = (props) => {
  return (
    <Card style={{ maxWidth: '345px', margin: '10px' }}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt="Thumbnail"
          height="140"
          image={ props.entry.thumbnail }
          title={ props.entry.title }
        />
        <CardContent style={{ flex: '1 0 auto', textAlign: 'left' }}>
          <Typography component="h5" variant="h5">
            { props.entry.title }
          </Typography>
          <Typography variant="subtitle1" color="textSecondary">
            { props.entry.duration }, by { props.entry.user }
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

const DesktopCard = (props) => {
  return(
    <Card style={{ display: 'flex', margin: '10px', minWidth: '50%', maxWidth: '90%', width: '800px' }}>
      <CardMedia 
        image={ props.entry.thumbnail }
        title={ props.entry.title }
        style={{ width: '151px', minWidth: '151px' }}
      />
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <CardContent style={{ flex: '1 0 auto', textAlign: 'left' }}>
          <Typography component="h5" variant="h5">
            { props.entry.title }
          </Typography>
          <Typography variant="subtitle1" color="textSecondary">
            { props.entry.duration }, by { props.entry.user }
          </Typography>
        </CardContent>
      </div>
    </Card>
  );
}

ReactDOM.render(<Add/>, document.getElementById('add'));
ReactDOM.render(<Playing/>, document.getElementById('playing'));
ReactDOM.render(<Queue/>, document.getElementById('queue'));
ReactDOM.render(<Hint/>, document.getElementById('hint'));
