import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import CardActionArea from '@material-ui/core/CardActionArea';

class Add extends React.Component {
  constructor(props) {
    super(props);
    var entries = document.querySelector('#addEntry').dataset.entries;
    var name = document.querySelector('#addEntry').dataset.name;
    this.state = {
      term: '',
      entries: JSON.parse(entries),
      name: name,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSelection = this.handleSelection.bind(this);
  }

  handleChange(event) {
    this.setState({
      term: event.target.value,
    }) 
  }

  handleSelection(selection) {
    var url = '/addtodatabase/' + this.state.name;
    var form = $('<form action="' + url + '" method="post">' +
      '<input name="title" value="' + selection.title + '" />' +
      '<input name="duration" value="' + selection.length + '" />' +
      '<input name="thumbnail" value="' + selection.thumbnail + '" />' +
      '<input name="videoId" value="' + selection.id + '" />' +
      '</form>');
    $('body').append(form);
    form.submit();
  }

  render() {
    if( window.innerWidth > 768 ) {
      return (
        <div>
          <form style={{ textAlign: 'center', marginBottom: '50px' }} method="POST" action=''>
            <div>
              <TextField id="search" name="search" label="Enter Search Term" variant="outlined" onChange={ this.handleChange } value={ this.state.term } required autoFocus/>
            </div>
            <div style={{ margin: '10px', textAlign: 'center' }}>
              <Button variant="contained" color="primary" type="submit">
                Search
              </Button>
            </div>
          </form>

          {this.state.entries.map(entry => (
            <Card style={{ display: 'flex', margin: '10px' }} onClick={() => this.handleSelection(entry)}>
              <CardMedia 
                //image="{ entry.thumbnail }"
                image={ entry.thumbnail }
                title="asdf"
                style={{ width: '151px', minWidth: '151px' }}
              />
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <CardContent style={{ flex: '1 0 auto' }}>
                  <Typography component="h5" variant="h5">
                    { entry.title }
                  </Typography>
                  <Typography variant="subtitle1" color="textSecondary">
                    { entry.length }, by { entry.uploader }
                  </Typography>
                </CardContent>
              </div>
            </Card>
          ))
          }
        </div>
      );
    }

    return (
      <div style={{
        marginTop: '20px'
      }}>
        <form style={{ textAlign: 'center' }} method="POST" action=''>
          <div>
            <TextField id="search" name="search" label="Enter Search Term" variant="outlined" onChange={ this.handleChange } value={ this.state.term } required/>
          </div>
          <div style={{ margin: '10px', textAlign: 'center' }}>
            <Button variant="contained" color="primary" type="submit">
              Search
            </Button>
          </div>
        </form>

        {this.state.entries.map(entry => (
          <Card style={{ maxWidth: '345px', margin: '10px' }} onClick={() => this.handleSelection(entry)}>
            <CardActionArea>
              <CardMedia
                component="img"
                alt="Contemplative Reptile"
                height="140"
                image={ entry.thumbnail }
                title="Contemplative Reptile"
              />
              <CardContent style={{ flex: '1 0 auto' }}>
                <Typography component="h5" variant="h5">
                  { entry.title }
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                  { entry.length }, by { entry.uploader }
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        ))
      }
      </div>
    );
  }
}

ReactDOM.render(<Add/>, document.getElementById('addEntry'));
