import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({name: event.target.value});
  }

  handleSubmit(event) {
    window.location = '/queue/' + this.state.name;
    event.preventDefault();
  }

  render () {
    return (
      <div style={{
        textAlign: 'center',
        marginTop: '20px',
      }}>
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <div>
            <TextField id="outlined-basic" label="Name" variant="outlined" value={this.state.name} onChange={this.handleChange} required autoFocus/>
          </div>
          <div style={{ marginTop: '10px', textAlign: 'center' }}>
            <Button variant="contained" color="primary" type="submit">
              Next
            </Button>
          </div>
        </form>
      </div>
    )
  }
}

ReactDOM.render(<NameForm/>, document.getElementById('NameForm'));
