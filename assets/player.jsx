import './styles/app.css';

// start the Stimulus application
import './bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';
import Alert from '@material-ui/lab/Alert';
import Button from '@material-ui/core/Button';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import LinearProgress from '@material-ui/core/LinearProgress';
import Slider from '@material-ui/core/Slider';
import {Howl, Howler} from 'howler';

class Player extends React.Component {
  constructor(props) {
    super(props);
    var id = document.querySelector('#player').dataset.id;
    var reload = document.querySelector('#player').dataset.reload;

    this.nextSong = this.nextSong.bind(this);
    this.reload = this.reload.bind(this);
    this.wait = this.wait.bind(this);
    this.play = this.play.bind(this);
    this.update = this.update.bind(this);
    this.onLoad = this.onLoad.bind(this);
    this.handleChange = this.handleChange.bind(this);

    if(id != 'none') {
        
      this.music = new Howl({ 
        src: id + '.mp3',
        onend: this.nextSong,
        onload: this.onLoad,
        autoplay: true,
      });

      this.state = {
        id: id,
        duration: 1,
        progress: 0,
      };
    } else {
      this.state = {
        id: id,
        duration: 0,
        progress: 0,
      };
    }

    if(reload) {
      this.reload();
    }
  }

  onLoad() {
    this.setState({
      id: this.state.id,
      duration: this.music.duration(0),
      progress: this.state.progress,
    });
  }

  nextSong() {
    window.location = '/player/' + this.state.id;
  }

  reload() {
    window.location = '/player/'
  }

  wait() {
    setTimeout(function(){
       window.location = '/player';
    }, 5000);

    return(
      <Alert severity="info">There's nothing in the queue</Alert>
    );  
  }

  componentDidMount() {
    if(this.state.id != 'none') {
      this.interval = setInterval(this.update, 1000);
    }
  }

  update() {
    var progress = this.music.seek() * 100 / this.state.duration;
    this.setState({ id: this.state.id, duration: this.state.duration, progress: progress });
  }

  handleChange (event, newValue) {
    this.setState({ id: this.state.id, duration: this.state.duration, progress: newValue });
    this.music.seek(newValue * this.state.duration / 100);
    event.preventDefault();
  };

  play() {
    return(
      <div style={{ textAlign: 'center' }}>
        <Slider value={this.state.progress} onChange={this.handleChange} aria-labelledby="continuous-slider" />
        <div style={{ marginTop: '20px' }}>
          {(() => {
            if(this.music.playing()) {
              return(<Button variant="contained" color="primary" onClick={ () => { this.music.pause() }} style={{ margin: '10px' }}> <PauseIcon /> </Button>);
            }
            return(<Button variant="contained" color="primary" onClick={ () => { this.music.play() }} style={{ margin: '10px' }}> <PlayArrowIcon /> </Button>);
          })()}
          <Button variant="contained" color="primary" onClick={ this.nextSong } style={{ margin: '10px' }}>
            Skip
          </Button>
          <Button variant="contained" color="primary" onClick={ this.reload } style={{ margin: '10px' }}>
            Reload
          </Button>
        </div>
      </div>
    );
  }

  render() {
    if(this.state.id == 'none') {
      return(this.wait());
    } return(this.play());
  }
}

ReactDOM.render(<Player/>, document.getElementById("player"));
