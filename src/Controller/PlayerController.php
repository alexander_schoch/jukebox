<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Repository\EntryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Routing\Annotation\Route;

class PlayerController extends AbstractController
{
    /**
     * @Route("/player/{id}", name="player")
     */
    public function index(EntryRepository $entryRepository, string $id, HubInterface $hub): Response
    {
        // update all clients, as stuff was probably changed
        $update = new UpdateController($hub, $entryRepository);
        $update->pushUpdate();

        $entries = $entryRepository->findAll();
        $nextId = 'none';
        $nextTitle = 'none';

        // download everything that should still be downloaded
        //for($i = 0; $i < count($entries); $i++)
        //{
        //  if(!$this->isAvailable($entries[$i]->getVideoId())) {
        //    $this->download($entries[$i]);
        //  }
        //}

        // if the site is not called by user, nothing should be removed
        if($id == 'none') {
          if(count($entries) != 0) {
            $nextId = $entries[0]->getVideoId();
            $nextTitle = $entries[0]->getTitle();
          }
        } else { // else, a song is over
          if(count($entries) != 0) {
            $toRemove = $entryRepository->findOneBy(['videoId' => $id]);
            $this->remove($toRemove);
            $em = $this->getDoctrine()->getManager();
            $em->remove($toRemove);
            $em->flush();
            if(count($entries) != 1) {
              $nextId = $entries[1]->getVideoId();
              $nextTitle = $entries[1]->getTitle();
            } 
          }
        }
        //$this->removeObsolete($entries);
        $returnvalue = $this->stall($nextId);
        if($id == 'none')
          $reload = false;
        else
          $reload = true;
        if($returnvalue) {
          return $this->render('player/index.html.twig', [
            'id' => $nextId,
            'title' => $nextTitle,
            'reload' => $reload,
          ]);
        } else {
          dump("false");
          //return new RedirectResponse('/player/' . $nextId);
        }
    }

    /**
     * @Route("/player", name="playerNoIndex")
     */
    public function noIndex(EntryRepository $entryRepository, HubInterface $hub): Response
    {
      return($this->index($entryRepository, 'none', $hub));
    }

    //public function download(Entry $entry): void
    //{
    //  array_push(
    //    $this->actions,
    //    "downloading " . $entry->getTitle() . " (" . $entry->getVideoId() . ")"
    //  );
    //  $id = $entry->getVideoId();
    //  exec('youtube-dl -x --audio-format=mp3 --id "https://youtube.com/watch?v=' . $id . '" > /dev/null &');
    //}

    public function remove(Entry $entry): void
    {
      $id = $entry->getVideoId();
      exec('rm ./' . $id . '.mp3');
    }

    public function removeFile(string $file): void
    {
      exec('rm ' . $file);
    }

    public function isAvailable(string $id): bool
    {
      if($id == 'none')
        return false;
      $files = glob('*.mp3');
      if(in_array($id . '.mp3', $files)) {
        return true;
      }
      return false;
    }

    public function removeObsolete(array $entries): void
    {
      $ids = array();
      foreach($entries as $entry) {
        array_push(
          $ids,
          $entry->getVideoId()
        );
      } 
      $files = glob('*.mp3');
      foreach($files as $file)
      {
        if(in_array(explode('.', $file)[0], $ids) === false) {
          $this->removeFile($file);
        }
      }
    }

    public function stall(string $id): bool
    {
      print_r('stalling');
      if($id == 'none')
        return true;
      $files = glob($id . '.*');
      $delay = 0;
      while (count($files) == 0) {
        sleep(1);
        $files = glob($id . '.*');
        // download failed. skip to next song
        if($delay > 5) {
          return false;
        }
      }
      while (count($files) > 1 || explode('.', $files[0])[1] != 'mp3')
      {
        sleep(1);
        $files = glob($id . '.*');
      } 
      return true;
    }
}
