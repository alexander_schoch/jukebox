<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

header("Access-Control-Allow-Origin: *");

class AddController extends AbstractController
{
    /**
     * @Route("/add/{name}", name="add")
     */
    public function index(string $name): Response
    {
        if(isset($_POST['search'])) {
            $entries = $this->getYoutubeEntries($_POST['search']);
        } else {
            $entries = array();
        }
        //dump(file_get_contents('https://archlinux.org'));
        return $this->render('add/index.html.twig', [
          'name' => $name,
          'entries' => json_encode($entries)
        ]);
    }

    public function getYoutubeEntries(string $search)
    {
      if(!isset($search)) {
        return array();
      }

      $url = "https://www.youtube.com/results?search_query=" . str_replace(" ", "+", $search);
      $html = file_get_contents($url);
      $relevantLine = explode(PHP_EOL, $html)[20];
      $red = explode('var ytInitialData = ', $relevantLine)[1];
      $json = explode(';</script><script nonce', $red)[0];
      $ob = json_decode($json);
      $contents = $ob->contents->twoColumnSearchResultsRenderer->primaryContents->sectionListRenderer->contents[0]->itemSectionRenderer->contents;
      $entries = array();
      $i = 0;

      while(count($entries) < 5) {
        if(property_exists($contents[$i], "videoRenderer")) {
          $vr = $contents[$i]->videoRenderer;
          array_push(
            $entries,
            array(
              'id' => $vr->videoId,
              'title' => $vr->title->runs[0]->text,
              'thumbnail' => $vr->thumbnail->thumbnails[count($vr->thumbnail->thumbnails) - 1]->url,
              'uploader' => $vr->ownerText->runs[0]->text,
              'length' => $vr->lengthText->simpleText,
            ) 
          ); 
        }
        $i++;
      }
      return $entries;
    }

    /**
     * @Route("/add", name="addNoName")
     */
    public function redirectBack(): RedirectResponse
    {
        return new RedirectResponse("/");
    }
}
