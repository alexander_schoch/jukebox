<?php

namespace App\Controller;

use App\Repository\EntryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

class UpdateController extends AbstractController
{
  public function __construct(HubInterface $hub, EntryRepository $entryRepository) 
  {
    $this->hub = $hub;
    $this->entryRepository = $entryRepository;
  }

  public function pushUpdate() {
      $qr = new QueueController($this->entryRepository);

      $entries = $qr->getEntries();
      $playing = $qr->getPlaying();

      $update = new Update(
          '/entries',
          $entries
      );

      $this->hub->publish($update);

      $update = new Update(
          '/playing',
          $playing, 
      );

      $this->hub->publish($update);

      return;
  }
}

