<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Repository\EntryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class QueueController extends AbstractController
{
    public function __construct(EntryRepository $entryRepository) {
        $this->entryRepository = $entryRepository;
        $this->serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
        return;
    }

    /**
     * @Route("/queue", name="queueRedirectBack")
     */
    public function goBack(): Response
    {
      return new RedirectResponse('/');
    }

    /**
     * @Route("/queue/{name}", name="queue")
     */
    public function index(string $name): Response
    {
        $json = $this->getEntries();
        $playing = $this->getPlaying();

        return $this->render('queue/index.html.twig', [
            'name' => $name,
            'entries' => $json,
            'playing' => $playing,
        ]);
    }

    public function getEntries() {
        $entries = $this->entryRepository->findAll();
        $json = $this->serializer->serialize(array_slice($entries, 1, 5, false), 'json');
        return $json;
    }

    public function getPlaying() {
        $entries = $this->entryRepository->findAll();
        if(count($entries) == 0) {
          $obj = array(
            'id' => 0,
            'title' => 'Add a new Entry by tapping on the plus sign',
            'user' => 'Alex the Admin',
            'duration' => 'now',
            'thumbnail' => '/media/signet.png',
            'videoId' => 'xxx',
            'isPlaying' => true,
          );
          return json_encode($obj);
        }
        return $this->serializer->serialize($entries[0], 'json');
    }

    /**
     * @Route("/remove/{id}", name="remove_entry")
     */
    public function remove(EntryRepository $entryRespository, int $id) {
      $entry = $entryRepository->findById($id)[0]; 
      $em = $this->getDoctrine->getManager();
      $em->remove($entry);
      $em->flush();

      $id = $entry->getVideoId();
      exec('rm ' . $id . '.mp3');
    }
}
