<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Repository\EntryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Routing\Annotation\Route;

use \Firebase\JWT\JWT;

class AddToDatabaseController extends AbstractController
{
    /**
     * @Route("/addtodatabase/{name}", name="add_to_database")
     */
    public function index(string $name, EntryRepository $entryRepository, HubInterface $hub): Response
    {

      if(isset($_POST['title'])) {
        $entry = new Entry();
        $entry->setTitle($_POST['title']); 
        $entry->setUser($name); 
        $entry->setDuration($_POST['duration']); 
        $entry->setThumbnail($_POST['thumbnail']); 
        $entry->setVideoId($_POST['videoId']); 
        $entry->setIsPlaying(false);

        if($this->download($entry)) {
          $em = $this->getDoctrine()->getManager();

          $em->persist($entry);
          $em->flush();
          $update = new UpdateController($hub, $entryRepository);
          $update->pushUpdate();
        }
      }

      return new RedirectResponse('/queue/' . $name);
    }

    public function download(Entry $entry): bool
    {
      $id = $entry->getVideoId();
      exec('youtube-dl -x --audio-format=mp3 --id "https://youtube.com/watch?v=' . $id . '" > /dev/null &');
      $files = glob($id . '.*');
      $delay = 0;
      // wait for three seconds to start the download. return false if it still didn't work (meaning that there was an error)
      //while(count($files) == 0)
      //{
      //  sleep(1);
      //  $files = glob($id . '.*');
      //  $delay++;
      //  if($delay > 3)
      //    return false;
      //}
      return true;
    }
}
