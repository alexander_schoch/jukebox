use \Firebase\JWT\JWT;

$key = "!ChangeMe!";
$payload = [
    'mercure' => [
        'publish' => ['*'],
    ],
];
$jwt = JWT::encode($payload, $key); // holds valid jwt now

echo ($jwt);
